import math

class Attributes:
    def __init__(self):
        self.stre = None
        self.cha = None
        self.dex = None
        self.con = None
        self.inte = None
        self.wis = None

    def getStre(self):
        return self.stre 

    def getCha(self):
        return self.cha    

    def getDex(self):
        return self.dex    

    def getCon(self):
        return self.con

    def getInte(self):
        return self.inte

    def getWis(self):
        return self.wis

    def getModstre(self):
        return math.floor((self.stre-10)/2)

    def getModCha(self):
        return math.floor((self.cha-10)/2) 

    def getModDex(self):
        return math.floor((self.dex-10)/2)

    def getModCon(self):
        return math.floor((self.con-10)/2)

    def getModinte(self):
        return math.floor((self.inte-10)/2)

    def getModWis(self):
        return math.floor((self.wis-10)/2)       


